package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Helpers {
    public void typeInField(String xpath, String value, ChromeDriver driver) throws  InterruptedException {
        String val = value;
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement element = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
//        WebElement element = driver.findElement(By.xpath(xpath));
        element.clear();

        for (int i = 0; i < val.length(); i++) {
            char c = val.charAt(i);
            String s = new StringBuilder().append(c).toString();
            element.sendKeys(s);
            Thread.sleep(10);
        }
    }

    public void typeInFieldByClass(String selector, String value, ChromeDriver driver) throws  InterruptedException {
        String val = value;
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement element = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.cssSelector(selector)));
//        WebElement element = driver.findElement(By.xpath(xpath));
        element.clear();

        for (int i = 0; i < val.length(); i++) {
            char c = val.charAt(i);
            String s = new StringBuilder().append(c).toString();
            element.sendKeys(s);
            Thread.sleep(10);
        }
    }

    public void clickHandlerElement(WebElement element, ChromeDriver driver){
        JavascriptExecutor executor = driver;
        executor.executeScript("arguments[0].click();", element);
    }


}

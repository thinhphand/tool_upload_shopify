package main;

import jdk.jfr.Timestamp;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import shopify.Login;
import utils.Helpers;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class Shopify {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver",
                "D:\\THINH\\T-Shirts\\Tools\\bitbucket\\tools-upload-shopify\\chromedriver_win32_78\\chromedriver.exe");

        Helpers helpers = new Helpers();
        JSONParser parser = new JSONParser();

        try {

            Object obj = parser.parse(new FileReader("D:\\THINH\\T-Shirts\\Tools\\bitbucket\\tools-upload-shopify\\data\\Shopify\\data.json"));

            JSONObject jsonObject = (JSONObject) obj;
            System.out.println(jsonObject);
            ChromeOptions options = new ChromeOptions();
            options.addArguments("user-data-dir=C:\\Users\\thinhphan.info\\AppData\\Local\\Google\\Chrome\\User Data");
            options.addArguments("--start-maximized");
            ChromeDriver driver = new ChromeDriver(options);
            //---> Loop list design.
            ArrayList<JSONObject> listDesign = (ArrayList) jsonObject.get("list_design");
            for (JSONObject objCamp : listDesign
            ) {
                driver.get("https://thinhcafustore.myshopify.com/admin/apps");

                //---> Waiting Customcat - Print on Demand visible
                WebDriverWait wait = new WebDriverWait(driver, 10);
                WebElement btnCustomcatElement = wait.until(
                        ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"app-title-link\"]")));
                btnCustomcatElement.click();

                //---> Click on Add Product button.
                WebElement btnAddProductElement = wait.until(
                        ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"AppFrameMain\"]/div/div/div/div[2]/div/div[1]/div/div[2]/button")));
                btnAddProductElement.click();
                Thread.sleep(1000);
                WebElement iFrame = driver.findElements(By.tagName("iframe")).get(0);
                driver.switchTo().frame(iFrame);
                Thread.sleep(1000);
                System.out.println(">>> Test frame: " + driver.findElementByXPath("//*[@id=\"dye-sub-select\"]").getText());
                WebElement btnDyeSubEl = driver.findElement(By.xpath("//*[@id=\"dye-sub-select\"]"));
                btnDyeSubEl.click();
                System.out.println(">>> Clicked on Dye Sublimation button.");

                //---> Searching mug product type
                helpers.typeInField("//*[@id=\"product-searchterm\"]", "11 oz. White Mug", driver);

                //---> Click on search icon
                driver.findElementByXPath("//*[@id=\"product-search-button\"]").click();

                WebElement mugProductOZ11 = wait.until(
                        ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"product-1005\"]")));
                mugProductOZ11.click();

                WebElement saveProductTitleModal = wait.until(
                        ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"product-modal-body\"]/div/div[2]/button")));
                saveProductTitleModal.click();
                Thread.sleep(2000);

                //---> Uploader start button
                WebElement uploaderStartBtn = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"uploader-start\"]")));
                uploaderStartBtn.click();

                //---> Save template
                WebElement saveTemplateBtn = wait.until(
                        ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"save-template-modal-body\"]/div/div[2]/button")));
                saveTemplateBtn.click();

                //---> Search by tag or filename
                String fileName = objCamp.get("file_name").toString();
                helpers.typeInField("//*[@id=\"design-searchterm\"]", fileName, driver);
                driver.findElementByXPath("//*[@id=\"design-search-button\"]").click();
                Thread.sleep(1000);
                driver.findElementByXPath("//*[@id=\"design-grid\"]/div").click();
                Thread.sleep(500);
                driver.findElementByXPath("/html/body/div/div[2]/div[3]/a").click();
                Thread.sleep(500);

                //---> Input title
                String mugTitle =  objCamp.get("title").toString() + " " +System.currentTimeMillis();
                helpers.typeInFieldByClass(".product-title.product-title-text.show", mugTitle, driver);
                System.out.println(">>> Typed campaign title");
                Thread.sleep(500);

                //---> Export product.
                driver.findElementByXPath("/html/body/div/div[1]/div[3]/a").click();
                Thread.sleep(5000);
                WebElement exportMoreBtn = wait.until(
                        ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"export-more-btn\"]/a")));
                System.out.println("Uploaded a file successfully.");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        catch (ParseException e) {
//            e.printStackTrace();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
}

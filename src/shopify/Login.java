package shopify;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Helpers;

public class Login {
    Helpers helper = new Helpers();

    public void login(ChromeDriver driver) throws InterruptedException {
        driver.get("https://thinhcafustore.myshopify.com/admin/apps/customcat-fulfillment");
        Thread.sleep(3000);

        //---> Send keys email
        inputEmail("//*[@id=\"account_email\"]", "thinhphan.info@gmail.com", driver);
        //---> Click Next button
        driver.findElementByXPath("//*[@id=\"body-content\"]/div[1]/div[2]/div/form/button").click();

        //---> Send keys password
        inputPassword("//*[@id=\"account_password\"]", "Thinh@131", driver);

        //---> Click on Submit button.
        clickOnLoginButton(driver, "//*[@id=\"login_form\"]/button");

    }

    public void inputEmail(String selector, String email, ChromeDriver driver) throws InterruptedException {
        //---> Send keys email
        helper.typeInField(selector, email, driver);
    }

    public void inputPassword(String selector, String password, ChromeDriver driver) throws InterruptedException {
        //---> Send keys password
        helper.typeInField(selector,
                password, driver);
    }

    public void clickOnLoginButton(ChromeDriver driver, String loginBtnSelector){
        driver.findElementByXPath(loginBtnSelector).click();
    }
}
